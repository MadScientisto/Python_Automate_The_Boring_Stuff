#! python3
# password_locker - An insecure locker program.

passwords = {'email': 'beepboop',
             'reddit': 'beepbeep',
             'stackoverflow': '1beep2boops'}
import sys, pyperclip
             
account = input("Whose account do you want the password for?")
if len(account) < 2:
    print("You done goofed it up")
    sys.exit()

if account in passwords:
        pyperclip.copy(passwords[account])
        print("Password for " + account + " copied to clipboard")
else:
        print("There is no account named " + account)
