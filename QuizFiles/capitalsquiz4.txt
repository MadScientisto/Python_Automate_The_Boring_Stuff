Name:

Date:

Period:

                    State Capitals Quiz (Form 4)

1. What is the capital of Colorado?
 A. Salt Lake City

 B. Springfield

 C. Montgomery

 D. Columbia

2. What is the capital of Montana?
 A. Montgomery

 B. Cheyenne

 C. Pierre

 D. Oklahoma City

3. What is the capital of Kentucky?
 A. Bismarck

 B. Montgomery

 C. Little Rock

 D. Pierre

4. What is the capital of Alabama?
 A. Olympia

 B. Boise

 C. Indianapolis

 D. Montgomery

5. What is the capital of Wyoming?
 A. Austin

 B. Denver

 C. Montgomery

 D. Indianapolis

6. What is the capital of Kansas?
 A. Montgomery

 B. Honolulu

 C. Helena

 D. Dover

7. What is the capital of Nevada?
 A. Salt Lake City

 B. Montgomery

 C. Pierre

 D. Lansing

8. What is the capital of Vermont?
 A. Columbus

 B. Oklahoma City

 C. Montgomery

 D. Saint Paul

9. What is the capital of Missouri?
 A. Oklahoma City

 B. Topeka

 C. Montgomery

 D. Concord

10. What is the capital of Minnesota?
 A. Richmond

 B. Albany

 C. Montgomery

 D. Trenton

11. What is the capital of Arizona?
 A. Montgomery

 B. Boston

 C. Montpelier

 D. Baton Rouge

12. What is the capital of New Hampshire?
 A. Phoenix

 B. Helena

 C. Little Rock

 D. Montgomery

13. What is the capital of Mississippi?
 A. Madison

 B. Columbia

 C. Montgomery

 D. Atlanta

14. What is the capital of Hawaii?
 A. Helena

 B. Montgomery

 C. Montpelier

 D. Juneau

15. What is the capital of New York?
 A. Madison

 B. Montgomery

 C. Jackson

 D. Bismarck

16. What is the capital of Alaska?
 A. Des Moines

 B. Topeka

 C. Baton Rouge

 D. Montgomery

17. What is the capital of North Carolina?
 A. Atlanta

 B. Albany

 C. Oklahoma City

 D. Montgomery

18. What is the capital of Delaware?
 A. Tallahassee

 B. Boise

 C. Montgomery

 D. Hartford

19. What is the capital of North Dakota?
 A. Baton Rouge

 B. Lansing

 C. Boise

 D. Montgomery

20. What is the capital of Louisiana?
 A. Montgomery

 B. Bismarck

 C. Cheyenne

 D. Salt Lake City

21. What is the capital of Utah?
 A. Hartford

 B. Dover

 C. Indianapolis

 D. Montgomery

22. What is the capital of Texas?
 A. Jackson

 B. Montgomery

 C. Saint Paul

 D. Sacramento

23. What is the capital of Indiana?
 A. Lincoln

 B. Atlanta

 C. Providence

 D. Montgomery

24. What is the capital of Pennsylvania?
 A. Montpelier

 B. Phoenix

 C. Bismarck

 D. Montgomery

25. What is the capital of South Carolina?
 A. Montgomery

 B. Albany

 C. Jackson

 D. Augusta

26. What is the capital of West Virginia?
 A. Austin

 B. Harrisburg

 C. Des Moines

 D. Montgomery

27. What is the capital of Arkansas?
 A. Nashville

 B. Baton Rouge

 C. Montgomery

 D. Concord

28. What is the capital of New Mexico?
 A. Columbus

 B. Cheyenne

 C. Montgomery

 D. Boise

29. What is the capital of Oregon?
 A. Boston

 B. Montgomery

 C. Montpelier

 D. Santa Fe

30. What is the capital of Idaho?
 A. Salt Lake City

 B. Olympia

 C. Hartford

 D. Montgomery

31. What is the capital of Maine?
 A. Montgomery

 B. Jefferson City

 C. Honolulu

 D. Carson City

32. What is the capital of Washington?
 A. Montgomery

 B. Bismarck

 C. Concord

 D. Honolulu

33. What is the capital of Virginia?
 A. Richmond

 B. Baton Rouge

 C. Montgomery

 D. Carson City

34. What is the capital of South Dakota?
 A. Indianapolis

 B. Dover

 C. Montgomery

 D. Madison

35. What is the capital of Oklahoma?
 A. Hartford

 B. Little Rock

 C. Baton Rouge

 D. Montgomery

36. What is the capital of Wisconsin?
 A. Providence

 B. Cheyenne

 C. Montgomery

 D. Boston

37. What is the capital of Massachusetts?
 A. Montgomery

 B. Providence

 C. Springfield

 D. Harrisburg

38. What is the capital of Nebraska?
 A. Jefferson City

 B. Charleston

 C. Montgomery

 D. Trenton

39. What is the capital of Iowa?
 A. Atlanta

 B. Montgomery

 C. Salem

 D. Boston

40. What is the capital of Georgia?
 A. Jefferson City

 B. Denver

 C. Bismarck

 D. Montgomery

41. What is the capital of New Jersey?
 A. Montgomery

 B. Dover

 C. Lincoln

 D. Carson City

42. What is the capital of Michigan?
 A. Boston

 B. Annapolis

 C. Oklahoma City

 D. Montgomery

43. What is the capital of California?
 A. Frankfort

 B. Santa Fe

 C. Montgomery

 D. Little Rock

44. What is the capital of Tennessee?
 A. Helena

 B. Montgomery

 C. Juneau

 D. Lincoln

45. What is the capital of Rhode Island?
 A. Indianapolis

 B. Boise

 C. Santa Fe

 D. Montgomery

46. What is the capital of Ohio?
 A. Oklahoma City

 B. Montgomery

 C. Augusta

 D. Nashville

47. What is the capital of Maryland?
 A. Nashville

 B. Annapolis

 C. Lansing

 D. Montgomery

48. What is the capital of Illinois?
 A. Montgomery

 B. Juneau

 C. Salt Lake City

 D. Lansing

49. What is the capital of Connecticut?
 A. Little Rock

 B. Madison

 C. Montgomery

 D. Denver

50. What is the capital of Florida?
 A. Carson City

 B. Little Rock

 C. Austin

 D. Montgomery

