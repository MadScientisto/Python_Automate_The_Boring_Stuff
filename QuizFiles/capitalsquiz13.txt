Name:

Date:

Period:

                    State Capitals Quiz (Form 13)

1. What is the capital of Rhode Island?
 A. Boise

 B. Pierre

 C. Raleigh

 D. Charleston

2. What is the capital of Missouri?
 A. Montpelier

 B. Pierre

 C. Cheyenne

 D. Columbus

3. What is the capital of Washington?
 A. Olympia

 B. Hartford

 C. Harrisburg

 D. Pierre

4. What is the capital of Hawaii?
 A. Pierre

 B. Baton Rouge

 C. Saint Paul

 D. Albany

5. What is the capital of Kansas?
 A. Boise

 B. Saint Paul

 C. Concord

 D. Pierre

6. What is the capital of Idaho?
 A. Pierre

 B. Topeka

 C. Tallahassee

 D. Raleigh

7. What is the capital of Wyoming?
 A. Carson City

 B. Pierre

 C. Columbia

 D. Albany

8. What is the capital of Connecticut?
 A. Raleigh

 B. Juneau

 C. Atlanta

 D. Pierre

9. What is the capital of Georgia?
 A. Raleigh

 B. Sacramento

 C. Pierre

 D. Annapolis

10. What is the capital of New Jersey?
 A. Pierre

 B. Columbus

 C. Boise

 D. Lansing

11. What is the capital of North Carolina?
 A. Concord

 B. Pierre

 C. Montpelier

 D. Jackson

12. What is the capital of South Carolina?
 A. Boston

 B. Sacramento

 C. Pierre

 D. Dover

13. What is the capital of South Dakota?
 A. Madison

 B. Albany

 C. Pierre

 D. Salt Lake City

14. What is the capital of West Virginia?
 A. Oklahoma City

 B. Harrisburg

 C. Pierre

 D. Atlanta

15. What is the capital of Illinois?
 A. Saint Paul

 B. Columbia

 C. Atlanta

 D. Pierre

16. What is the capital of Vermont?
 A. Topeka

 B. Montgomery

 C. Pierre

 D. Madison

17. What is the capital of Kentucky?
 A. Little Rock

 B. Columbus

 C. Phoenix

 D. Pierre

18. What is the capital of Michigan?
 A. Pierre

 B. Carson City

 C. Little Rock

 D. Charleston

19. What is the capital of Louisiana?
 A. Saint Paul

 B. Denver

 C. Montpelier

 D. Pierre

20. What is the capital of New Hampshire?
 A. Santa Fe

 B. Pierre

 C. Lincoln

 D. Bismarck

21. What is the capital of California?
 A. Juneau

 B. Austin

 C. Pierre

 D. Annapolis

22. What is the capital of Ohio?
 A. Frankfort

 B. Pierre

 C. Juneau

 D. Jackson

23. What is the capital of New Mexico?
 A. Montpelier

 B. Phoenix

 C. Pierre

 D. Lansing

24. What is the capital of Alabama?
 A. Cheyenne

 B. Charleston

 C. Pierre

 D. Phoenix

25. What is the capital of Maryland?
 A. Nashville

 B. Olympia

 C. Jefferson City

 D. Pierre

26. What is the capital of Maine?
 A. Augusta

 B. Raleigh

 C. Phoenix

 D. Pierre

27. What is the capital of Delaware?
 A. Denver

 B. Atlanta

 C. Pierre

 D. Austin

28. What is the capital of Montana?
 A. Lansing

 B. Pierre

 C. Jackson

 D. Olympia

29. What is the capital of Oregon?
 A. Columbia

 B. Pierre

 C. Jackson

 D. Santa Fe

30. What is the capital of Minnesota?
 A. Pierre

 B. Boston

 C. Lincoln

 D. Salt Lake City

31. What is the capital of Alaska?
 A. Pierre

 B. Denver

 C. Baton Rouge

 D. Atlanta

32. What is the capital of Massachusetts?
 A. Jackson

 B. Pierre

 C. Atlanta

 D. Charleston

33. What is the capital of Mississippi?
 A. Pierre

 B. Columbia

 C. Boise

 D. Tallahassee

34. What is the capital of Iowa?
 A. Pierre

 B. Frankfort

 C. Olympia

 D. Denver

35. What is the capital of Colorado?
 A. Dover

 B. Pierre

 C. Charleston

 D. Denver

36. What is the capital of Nebraska?
 A. Pierre

 B. Austin

 C. Des Moines

 D. Little Rock

37. What is the capital of Arizona?
 A. Dover

 B. Montgomery

 C. Pierre

 D. Honolulu

38. What is the capital of Arkansas?
 A. Helena

 B. Dover

 C. Pierre

 D. Providence

39. What is the capital of Nevada?
 A. Topeka

 B. Providence

 C. Pierre

 D. Hartford

40. What is the capital of Utah?
 A. Phoenix

 B. Pierre

 C. Little Rock

 D. Harrisburg

41. What is the capital of Oklahoma?
 A. Pierre

 B. Honolulu

 C. Lincoln

 D. Frankfort

42. What is the capital of Florida?
 A. Columbus

 B. Pierre

 C. Baton Rouge

 D. Concord

43. What is the capital of Wisconsin?
 A. Raleigh

 B. Pierre

 C. Columbia

 D. Springfield

44. What is the capital of Tennessee?
 A. Helena

 B. Trenton

 C. Augusta

 D. Pierre

45. What is the capital of North Dakota?
 A. Pierre

 B. Sacramento

 C. Montpelier

 D. Juneau

46. What is the capital of New York?
 A. Jackson

 B. Olympia

 C. Columbus

 D. Pierre

47. What is the capital of Virginia?
 A. Pierre

 B. Madison

 C. Carson City

 D. Olympia

48. What is the capital of Pennsylvania?
 A. Pierre

 B. Annapolis

 C. Boise

 D. Columbia

49. What is the capital of Texas?
 A. Austin

 B. Pierre

 C. Augusta

 D. Springfield

50. What is the capital of Indiana?
 A. Pierre

 B. Carson City

 C. Montgomery

 D. Annapolis

