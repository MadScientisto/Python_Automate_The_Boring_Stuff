Name:

Date:

Period:

                    State Capitals Quiz (Form 3)

1. What is the capital of North Carolina?
 A. Cheyenne

 B. Topeka

 C. Concord

 D. Springfield

2. What is the capital of North Dakota?
 A. Springfield

 B. Olympia

 C. Boise

 D. Frankfort

3. What is the capital of Illinois?
 A. Juneau

 B. Salem

 C. Springfield

 D. Richmond

4. What is the capital of California?
 A. Indianapolis

 B. Pierre

 C. Springfield

 D. Phoenix

5. What is the capital of Oregon?
 A. Juneau

 B. Springfield

 C. Boise

 D. Boston

6. What is the capital of Montana?
 A. Helena

 B. Santa Fe

 C. Boston

 D. Springfield

7. What is the capital of New Hampshire?
 A. Frankfort

 B. Santa Fe

 C. Nashville

 D. Springfield

8. What is the capital of Alaska?
 A. Cheyenne

 B. Denver

 C. Springfield

 D. Salt Lake City

9. What is the capital of South Dakota?
 A. Madison

 B. Springfield

 C. Denver

 D. Concord

10. What is the capital of Nevada?
 A. Montgomery

 B. Richmond

 C. Springfield

 D. Saint Paul

11. What is the capital of Minnesota?
 A. Boston

 B. Springfield

 C. Nashville

 D. Augusta

12. What is the capital of Colorado?
 A. Sacramento

 B. Topeka

 C. Boston

 D. Springfield

13. What is the capital of Connecticut?
 A. Carson City

 B. Springfield

 C. Bismarck

 D. Saint Paul

14. What is the capital of Michigan?
 A. Montgomery

 B. Springfield

 C. Concord

 D. Raleigh

15. What is the capital of Ohio?
 A. Helena

 B. Nashville

 C. Raleigh

 D. Springfield

16. What is the capital of Kansas?
 A. Atlanta

 B. Springfield

 C. Madison

 D. Nashville

17. What is the capital of Alabama?
 A. Olympia

 B. Boise

 C. Bismarck

 D. Springfield

18. What is the capital of Tennessee?
 A. Raleigh

 B. Indianapolis

 C. Springfield

 D. Trenton

19. What is the capital of Oklahoma?
 A. Bismarck

 B. Madison

 C. Springfield

 D. Providence

20. What is the capital of Nebraska?
 A. Harrisburg

 B. Little Rock

 C. Boise

 D. Springfield

21. What is the capital of Missouri?
 A. Springfield

 B. Carson City

 C. Juneau

 D. Madison

22. What is the capital of Maine?
 A. Austin

 B. Trenton

 C. Springfield

 D. Topeka

23. What is the capital of Arkansas?
 A. Honolulu

 B. Jackson

 C. Springfield

 D. Pierre

24. What is the capital of Georgia?
 A. Charleston

 B. Topeka

 C. Springfield

 D. Augusta

25. What is the capital of New Mexico?
 A. Springfield

 B. Cheyenne

 C. Oklahoma City

 D. Annapolis

26. What is the capital of Pennsylvania?
 A. Olympia

 B. Springfield

 C. Harrisburg

 D. Providence

27. What is the capital of Virginia?
 A. Raleigh

 B. Salt Lake City

 C. Lansing

 D. Springfield

28. What is the capital of Florida?
 A. Boise

 B. Saint Paul

 C. Harrisburg

 D. Springfield

29. What is the capital of Hawaii?
 A. Dover

 B. Springfield

 C. Juneau

 D. Bismarck

30. What is the capital of Arizona?
 A. Lincoln

 B. Santa Fe

 C. Montpelier

 D. Springfield

31. What is the capital of Louisiana?
 A. Springfield

 B. Baton Rouge

 C. Albany

 D. Montpelier

32. What is the capital of Washington?
 A. Little Rock

 B. Hartford

 C. Springfield

 D. Bismarck

33. What is the capital of Massachusetts?
 A. Santa Fe

 B. Columbus

 C. Springfield

 D. Cheyenne

34. What is the capital of Maryland?
 A. Honolulu

 B. Bismarck

 C. Hartford

 D. Springfield

35. What is the capital of Idaho?
 A. Pierre

 B. Springfield

 C. Charleston

 D. Salem

36. What is the capital of Rhode Island?
 A. Montgomery

 B. Austin

 C. Springfield

 D. Albany

37. What is the capital of Delaware?
 A. Atlanta

 B. Springfield

 C. Juneau

 D. Albany

38. What is the capital of South Carolina?
 A. Topeka

 B. Springfield

 C. Providence

 D. Santa Fe

39. What is the capital of New Jersey?
 A. Salt Lake City

 B. Harrisburg

 C. Springfield

 D. Montpelier

40. What is the capital of Vermont?
 A. Springfield

 B. Bismarck

 C. Topeka

 D. Saint Paul

41. What is the capital of Iowa?
 A. Madison

 B. Richmond

 C. Concord

 D. Springfield

42. What is the capital of Indiana?
 A. Saint Paul

 B. Salem

 C. Nashville

 D. Springfield

43. What is the capital of Wyoming?
 A. Honolulu

 B. Boise

 C. Augusta

 D. Springfield

44. What is the capital of Wisconsin?
 A. Springfield

 B. Jackson

 C. Cheyenne

 D. Topeka

45. What is the capital of Kentucky?
 A. Indianapolis

 B. Springfield

 C. Frankfort

 D. Annapolis

46. What is the capital of Utah?
 A. Carson City

 B. Concord

 C. Albany

 D. Springfield

47. What is the capital of New York?
 A. Phoenix

 B. Springfield

 C. Trenton

 D. Saint Paul

48. What is the capital of West Virginia?
 A. Charleston

 B. Lansing

 C. Honolulu

 D. Springfield

49. What is the capital of Mississippi?
 A. Pierre

 B. Concord

 C. Springfield

 D. Frankfort

50. What is the capital of Texas?
 A. Salem

 B. Nashville

 C. Providence

 D. Springfield

