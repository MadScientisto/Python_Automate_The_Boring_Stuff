#! python3
# 2048pilot.py - Loops through key strokes in 2048 until it gets required high score.

import time,sys,random
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

browser = webdriver.Chrome()
browser.get("https://gabrielecirulli.github.io/2048/")
target = int(sys.argv[1])
start = time.time()

while True:
    
    htmlElem = browser.find_element_by_tag_name("html")
    gameover = browser.find_element_by_class_name("lower")
    score = browser.find_element_by_class_name("score-container")
    while not gameover.is_displayed() :
        choice = random.randint(1,10)
        if choice < 9:
            if choice < 5:
                htmlElem.send_keys(Keys.UP)
            else:         
                htmlElem.send_keys(Keys.RIGHT)
        else:
            if choice == 9:
                htmlElem.send_keys(Keys.LEFT)  
            else:
                htmlElem.send_keys(Keys.DOWN)

    print(score.text)
    if int(score.text) >= target:
        end = time.time()
        elapsed = (end - start)
        print("Wooho! You done did it!")
        print("Target was reached in {0:.2f} seconds.".format(elapsed))
        break
    
    browser.refresh()
    