#! python3
# Takes a text file as an argument, reads the text file, finds specified keywords and replaces them with user input.

import re, sys, os

#Opens the file
originalFile = open(str(sys.argv[1]))
fileText = originalFile.read()

#Finds matches of the keywords
keywordsRegex = re.compile("ADJECTIVE|NOUN|ADVERB|VERB|")
finds = list(filter(None,keywordsRegex.findall(fileText)))
for match in finds:
    fileText = fileText.replace(match,input("Enter a %s: " % (match.lower())),1)

#Creates new file and prints the new text
newFile = open("new"+str(sys.argv[1]),"w")
newFile.write(fileText)
print(fileText)

newFile.close()
originalFile.close()