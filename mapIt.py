#! python3
# mapIt.py - Launches a map in the browser using an address from the command line or clipboard. 

import sys,webbrowser,pyperclip

if len(sys.argv) > 1:
    #Get addr from command line.
    address = " ".join(sys.argv[1:])
else:
    #Get addr from clipboard.
    address = pyperclip.paste()
print(address)
webbrowser.open('https://www.google.com/maps/place/' + address)